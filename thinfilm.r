rm(list = ls())

# LIBRARIES ################################

library(httpgd)
library(magicaxis)
library(chemCal)
source("minorticks.r")


# DATA PARSING ##################################

# function to read data from multiple files
read.tables <- function(file.names, ...) {
    require(plyr)
    ldply(file.names, function(fn) data.frame(Filename=fn, read.table(fn, skip = 1, ...)))
}



data_location <- "data/ThinFilms/IV/"
filenames_dark <- c() # constructing the dark file name vector
for (device in 1:5) {
   for (pixel in 1:6) {
        filename <- sprintf("s%s_",toString(device))
        filename <- paste(data_location, filename, sprintf("%sRDARK.dat", toString(pixel)), sep = "")
        filenames_dark <- c(filenames_dark, filename)
   }
}

filenames_light <- c() #constructing the light file name vector
for (device in 1:5) {
   for (pixel in 1:6) {
        filename <- sprintf("s%s_",toString(device))
        filename <- paste(data_location, filename, sprintf("%sRLIGHT.dat", toString(pixel)), sep = "")
        filenames_light <- c(filenames_light, filename)
   }
}

data_dark <- read.tables(filenames_dark) # extracting the data
data_light <- read.tables(filenames_light) # extracting the data

rm(filenames_dark) # cleanup
rm(filenames_light)

idx_file_change <- c() # to store locations where new plot starts
i <- 2
while (i <= length(data_dark[,1])) { # storing them by looking at file name in matrix
   filename <- data_dark[i, 1]
   filename_previous <- data_dark[i - 1, 1]
   if(filename != filename_previous){
      idx_file_change <- c(idx_file_change, i) #store location where it changes
   }
   i <- i + 1
}

idx_file_change <- append(idx_file_change, length(data_dark[,1])) # adding last number
pixel_area <- 0.16 #in cm^2


#plotting

for (i in 1:29) { # plotting all pixels
   range <- idx_file_change[i]:idx_file_change[i + 1]
   voltage <- data_dark[range, 2]
   current_dark <- data_dark[range, 3]
   current_light <- data_light[range, 3]

   current <- current_light - current_dark
   current_density <- current / pixel_area

   plot(voltage, current_density,
      type = "l",
      xlab = "Voltage [V]",
      ylab = "",
      xaxt = "n",
      yaxt = "n"
      
   )
   mtext(expression("Current Density"~"["*A/cm^2*"]"),2, padj = -2)
   magaxis(2, hersh = TRUE, majorn = 5, minorn = 10, cex.axis = 1.1)
   magaxis(1, hersh = TRUE, majorn = 5, minorn = 5, cex.axis = 1.1)

}