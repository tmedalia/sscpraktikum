rm(list=ls())


# LIBRARIES ################################

library(httpgd)
library(magicaxis)
library(chemCal)
source("minorticks.r")

# ABSORPTION SPECTROSCOPY DATA ################################

datafilm <- read.csv(file = "data/Absorption/Film_1.csv", skip = 1, sep = ";")
dataSC <- read.csv(file = "data/Absorption/singlecrystal_1.csv", skip = 1, sep = ";")

# CONSTANTS #############################

h <- 4.135667696e-15 #planck constant in eV * s
c <- 299792458 #speed of light in m/s


# HARVESTING FILM DATA ###################

wl <- datafilm[,1]
abs <- datafilm[,2]

en <- h*c / (wl*1e-9) #calculating the energy corresponding to every wavelength in eV
d <- 5e-4 #thickness in centimeters (5 um)
alpha <- 2.303 * abs / d


#FILM PLOTS LAYOUT SETTINGS ###############################

layout_matrix <- matrix(c(1,1,2,2,3,3,4,4), nrow = 2, byrow = TRUE)
layout(layout_matrix)


# Wavelength vs absorbance plot #######################

if (FALSE){ # Used to plot or not
plot(wl, abs,
    type = "l",
    xlab = "Wavelength [nm]",
    ylab = "Absorbance",
) #plot absorbance
}


# FILM ENERGY VS ABSORBANCE PLOT ##################################

plot(en, abs, 
    xlab = "Energy [eV]",
    ylab = "Absorbance",
    type = "l",
    xaxt = "n",
    yaxt = "n"
)
magaxis(2, hersh = TRUE, minorn = 5, cex.axis = 1)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 2, cex.axis = 1.1)

# FILM ENERGY VS ABSORBANCE COEFFICIENT PLOT ########################

plot(en, alpha, type = "l",
    xlab = "Energy [eV]",
    ylab = expression(alpha * "[cm" ^-1 * "]"),
    xaxt = "n",
    yaxt = "n"
)


magaxis(2, hersh = TRUE, majorn = 5, minorn = 10, cex.axis = 1.1)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 5, cex.axis = 1.1)



#FILM TAUC PLOT ##############################

y <- (alpha * h * (c / (wl * 1e-9)))^0.5 #calculate y of tauc plot with r = 2

plot(en, y,
    xlab = "Energy [eV]",
    ylab = '',
    type = "l",
    xaxt = "n",
    yaxt = "n"
)

mtext(expression((alpha * h * nu)^0.5), side = 2, padj = -1.5)
magaxis(2, hersh = TRUE, minorn = 5, cex.axis = 1.1)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 2, cex.axis = 1.1)

idx <- which(en >= 2.31 & en <= 2.361)

points(en[idx], y[idx],
    col = "red"
)

model <- lm(y[idx] ~ en[idx])
abline(model)
gap <- inverse.predict(model, 0)$Prediction # predict intercept of x axis


text(gap + .15, y = 50,
    bquote(E[g] * "=" * " " * .(round(gap, 2)) * " " * eV),
    cex = 1.2
)

## FILM URBACH ENERGY ########################

plot(en, log(alpha),
    type = "l",
    xaxt = "n",
    yaxt = "n",
    xlab = "Energy [eV]",
    xlim = c(2.2, 2.5),
    ylim = c(6.5, 10),
    ylab = expression(ln~(alpha))
)

magaxis(1, hersh = TRUE, majorn = 5, minorn = 2)
magaxis(2, hersh = TRUE, minorn = 5)
points(en[idx], log(alpha[idx]),
    col = "red"
)

urbach_model <- lm(log(alpha[idx]) ~ en[idx])
slope <- coef(urbach_model)[2]
urbach_energy <- 1 / slope

abline(urbach_model)
text(2.4, 8, bquote(E[U]~"="~.(round(urbach_energy*1000, 1))~"meV"))

## EXPORTING TO PDF AND RESETTING DEVICE #####################

dev.copy2pdf(file = "plots/Absorption/Molten_film_absorption2.pdf", width = 8, height = 6)
par(mfrow = c(1, 1))
layout_matrix <- matrix(c(1,1,2,2,3,3,4,4), nrow = 2, byrow = TRUE)
layout(layout_matrix)
# SINGLE CRYSTAL DATA HARVESTING #####################

wl <- dataSC[,1]
abs <- dataSC[,2]

en <- h*c / (wl*1e-9) #calculating the energy corresponding to every wavelength in eV
d <- 0.25 #thickness of crystal in centimeters (2.5 mm)
alpha <- 2.303 * abs / d # Absorption coefficient in cm^-1

# CRYSTAL ENERGY VS ABSORBANCE PLOT ##################################

plot(en, abs, 
    xlab = "Energy [eV]",
    ylab = "Absorbance",
    type = "l",
    xaxt = "n",
    yaxt = "n"
)
magaxis(2, hersh = TRUE, minorn = 5, cex.axis = 1)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 2, cex.axis = 1.1)

# CRYSTAL ENERGY VS ABSORBANCE COEFFICIENT PLOT ########################

plot(en, alpha, type = "l",
    xlab = "Energy [eV]",
    ylab = expression(alpha * "[cm" ^-1 * "]"),
    xaxt = "n",
    yaxt = "n"
)

magaxis(2, hersh = TRUE, minorn = 10, cex.axis = 1.1)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 2, cex.axis = 1.1)

#CRYSTAL TAUC PLOT ##############################

y <- (alpha * h * (c / (wl * 1e-9)))^2 #calculate y of tauc plot with r = 1/2

plot(en, y,
    xlab = "Energy [eV]",
    ylab = '',
    type = "l",
    xaxt = "n",
    yaxt = "n",
    xlim = c(2.2,2.5),
    ylim = c(0, 2e4)
)

mtext(expression((alpha * h * nu)^2), side = 2, padj = -1.5, cex = .8)
magaxis(2, hersh = TRUE, minorn = 5, cex.axis = 1.1)
magaxis(1, hersh = TRUE, majorn = 5, minorn = 2, cex.axis = 1.1)

idx <- which(en >= 2.255 & en <= 2.27)

points(en[idx], y[idx],
    col = "red"
)

model <- lm(y[idx] ~ en[idx])
abline(model, col = "blue")
gap <- inverse.predict(model, 0)$Prediction # predict intercept of x axis

text(gap + .05, y = 1000,
    bquote(E[g] * "=" * " " * .(round(gap, 2)) * " " * eV),
    cex = 1.2
)


## CRYSTAL URBACH ENERGY

plot(en, log(alpha),
    type = "l",
    xaxt = "n",
    yaxt = "n",
    xlab = "Energy [eV]",
    ylab = expression(ln(alpha)),
    xlim = c(2, 2.5),
    # ylim = c(6.5, 10),
)

idx <- which(en >= 2.23 & en <= 2.267)

magaxis(1, hersh = TRUE, majorn = 5, minorn = 2)
magaxis(2, hersh = TRUE, minorn = 5)
points(en[idx], log(alpha[idx]),
    col = "red"
)

urbach_model <- lm(log(alpha[idx]) ~ en[idx])
slope <- coef(urbach_model)[2]
urbach_energy <- 1 / slope

abline(urbach_model, col = "blue")
text(2.3, 2, bquote(E[U]~"="~.(round(urbach_energy*1000, 1))~"meV"))

dev.copy2pdf(file = "plots/Absorption/SC_absorption2.pdf", width = 8, height = 6)